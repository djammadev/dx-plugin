import { flags, SfdxCommand, core } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as _ from 'lodash';
const path = require("path");

const fs = require("fs");
var copydir = require("copy-dir");

const util = require("util");
const exec = util.promisify(require("child_process").exec);
var glob = require("glob");

const pairStatResources = "staticresources";
const pairStatResourcesRegExp = new RegExp(pairStatResources);
const pairAuraRegExp = new RegExp("aura");
const pairLwcRegExp = new RegExp("lwc");

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const diffMessages = Messages.loadMessages('dforce', 'diff');

export interface Types {
  members: string[];
  name: string;
}

export interface DiffFileStatus {
  revisionFrom: string;
  revisionTo: string;
  path: string;
  status: string;
  renamedPath?: string;
}

export interface DiffFile {
  deleted: DiffFileStatus[];
  addedEdited: DiffFileStatus[];
}

export default class Diff extends SfdxCommand {

  public static description = diffMessages.getMessage('commandDescription');

  public static examples = [
    `
    $ sfdx gforce:project:diff --difffile DiffFileName --output OutputFolder
    `,
    `
    $ sfdx gforce:project:diff -s sourceRevision -t targetRevision -d OutputFolder
    `
  ];

  protected static flagsConfig = {
    // flag with a value (-n, --name=VALUE)
    difffile: flags.string({ char: 'f', description: diffMessages.getMessage('diffFileFlagDescription') }),
    source: flags.string({ char: 's', description: diffMessages.getMessage('sourceFlagDescription') }),
    target: flags.string({ char: 't', description: diffMessages.getMessage('targetFlagDescription') }),
    output: flags.string({ char: 'd', description: diffMessages.getMessage('outputFlagDescription'), required: true })
  };

  // Comment this out if your command does not require an org username
  // protected static requiresUsername = false;

  // Comment this out if your command does not support a hub org username
  // protected static supportsDevhubUsername = true;

  // Set this to true if your command requires a project workspace; 'requiresProject' is false by default
  protected static requiresProject = true;

  async execShelCommand(command: string): Promise<string> {
    if (command === "") {
      return "";
    }
    const { stdout } = await exec(command, { maxBuffer: 1024 * 1000 });
    return stdout;
  }

  public parseContent(fileContents): DiffFile {
    //const startRegEx = /^:|^$/;
    //const statusRegEx = /^[A]$|^[M]$/;
    const statusRegEx = /\sA\t|\sM\t|\sD\t/;
    const renamedRegEx = /\sR[0-9]{3}\t|\sC[0-9]{3}\t/;
    const tabRegEx = /\t/;
    const deletedFileRegEx = new RegExp(/\sD\t/);
    const editedFileRegEx = new RegExp(/\sM\t/);
    const lineBreakRegEx = /\r?\n|\r|( $)/;

    var diffFile: DiffFile = {
      deleted: [],
      addedEdited: []
    };

    for (var i = 0; i < fileContents.length; i++) {
      if (statusRegEx.test(fileContents[i])) {
        let lineParts = fileContents[i].split(statusRegEx);

        let finalPath: string = path.join(
          ".",
          lineParts[1].replace(lineBreakRegEx, "")
        );
        finalPath = finalPath.trim();
        finalPath = finalPath.replace("\\303\\251", "é");
        if (finalPath.startsWith('"')) {
          finalPath = finalPath.substring(1, finalPath.length - 1);
        }
        if (finalPath.startsWith("'")) {
          finalPath = finalPath.substring(1, finalPath.length - 1);
        }
        let revisionPart = lineParts[0].split(/\t|\s/);

        if (deletedFileRegEx.test(fileContents[i])) {
          //Deleted
          diffFile.deleted.push({
            revisionFrom: revisionPart[2].substring(0, 9),
            revisionTo: revisionPart[3].substring(0, 9),
            path: finalPath,
            status: "D"
          });
        } else {
          // Added or edited
          let status = "A";
          if (editedFileRegEx.test(fileContents[i])) {
            status = "M";
          }
          diffFile.addedEdited.push({
            revisionFrom: revisionPart[2].substring(0, 9),
            revisionTo: revisionPart[3].substring(0, 9),
            path: finalPath,
            status: status
          });
        }
      } else if (renamedRegEx.test(fileContents[i])) {
        var lineParts = fileContents[i].split(renamedRegEx);

        var pathsParts = path.join(".", lineParts[1].trim());
        pathsParts = pathsParts.replace("\\303\\251", "é");
        let revisionPart = lineParts[0].split(/\t|\s/);

        var paths = pathsParts.split(tabRegEx);
        var finalPath = paths[1];

        diffFile.addedEdited.push({
          revisionFrom: "000000000",
          revisionTo: revisionPart[3],
          renamedPath: paths[0].trim(),
          path: finalPath,
          status: "M"
        });

        //allow deletion of renamed components
        diffFile.deleted.push({
          revisionFrom: revisionPart[2],
          revisionTo: "000000000",
          path: paths[0],
          status: "D"
        });

      }
    }
    return diffFile;
  }

  private isBunfle(filePath) {
    return pairAuraRegExp.test(filePath) || pairLwcRegExp.test(filePath);
  }

  private async copyBundle(inputFileDir, outputFileDir) {
    copydir.sync(inputFileDir, outputFileDir);
  }

  private isStaticResource(filePath) {
    return pairStatResourcesRegExp.test(filePath);
  }

  public static deleteFolderRecursive(folder) {
    if (fs.existsSync(folder)) {
      fs.readdirSync(folder).forEach(function (file, index) {
        var curPath = path.join(folder, file);
        if (fs.lstatSync(curPath).isDirectory()) { // recurse
          //console.log("Delete recursively");
          Diff.deleteFolderRecursive(curPath);
        } else { // delete file
          //console.log("Delete file "+ curPath);
          if(!curPath.endsWith('.gitkeep')) {
            fs.unlinkSync(curPath);
          }
        }
      });
      //console.log("delete folder "+ folder);
      //fs.rmdirSync(folder);
    }
  }

  public async run(): Promise<AnyJson> {
    const file = this.flags.difffile;
    const source = this.flags.source;
    const target = this.flags.target;
    const output = this.flags.output;
    var copyOutputFolder = output;
    if (!file && !(source && target)) {
      throw new core.SfdxError("difffile or (source and target) are required");
    }
    Diff.deleteFolderRecursive(path.join(output, 'force-app')); // remote output dir before copy new files
    if (!fs.existsSync(output)) {
      fs.mkdirSync(output); // create output dir.
    }
    let data;
    if (file) {
      let result = await core.fs.readFile(file);
      data = result.toString();
    } else {
      data = await this.execShelCommand(
        "git diff --raw " + source + "..." + target
      );
    }
    //console.log(data);
    let destructivePackageObj: Types[] = new Array();
    const sepRegex = /\n|\r/;
    let content = data.split(sepRegex);
    let diffFile: DiffFile = this.parseContent(content);
    let filesToCopy = diffFile.addedEdited;
    let deletedFiles = diffFile.deleted;
    /*if (deletedFiles.length > 0) {
      if (fs.existsSync('destructiveChangesPre.xml')) {
        fs.copyFileSync('destructiveChangesPre.xml', output);
      }
      if (fs.existsSync('destructiveChangesPost.xml')) {
        fs.copyFileSync('destructiveChangesPost.xml', output);
      }
    }*/
    //const pairAuaraRegExp = new RegExp("aura");
    // Return an object to be displayed with --json
    for (var i = 0; i < filesToCopy.length; i++) {
      let filePath = filesToCopy[i].path;
      if (!filePath.startsWith('force-app')) {
        continue;
      }
      let filePathParts = filePath.split(path.sep);
      let outputFileDir = output;
      let inputFileDir = '';
      for (var j = 0; j < filePathParts.length - 1; j++) {
        let folder = filePathParts[j].replace('"', "");
        outputFileDir = path.join(outputFileDir, folder);
        inputFileDir = path.join(inputFileDir, folder);
      }
      await core.fs.mkdirp(outputFileDir);
      let fileName = filePathParts[filePathParts.length - 1].replace('"', "");
      //let outputFile = path.join(outputFileDir, fileName);
      // console.log('Copying ' + filePath + ' --> ' + outputFile);
      if (this.isBunfle(filePath)) {
        this.copyBundle(inputFileDir, outputFileDir);
      }
      else if (filePath.endsWith("Translation-meta.xml") && filePath.indexOf("globalValueSet") < 0) {
        var parentFolder = filePathParts[filePathParts.length - 2];
        var objectTranslation = parentFolder + ".objectTranslation-meta.xml";
        var outputPath = path.join(outputFileDir, objectTranslation);
        var sourceFile = filePath.replace(fileName, objectTranslation);
        if (fs.existsSync(sourceFile) == true) {
          fs.copyFileSync(sourceFile, outputPath);
        }
      }
      //FOR STATIC RESOURCES - WHERE THE CORRESPONDING DIRECTORY + THE ROOT META FILE HAS TO BE RETRIEVED
      else if (this.isStaticResource(filePath)) {
        outputFileDir = path.join(".", copyOutputFolder);
        var srcFolder = ".";
        var staticRecourceRoot = "";
        var resourceFile = "";
        for (var k = 0; k < filePathParts.length; k++) {
          outputFileDir = path.join(outputFileDir, filePathParts[k]);
          srcFolder = path.join(srcFolder, filePathParts[k]);
          if (filePathParts[k] === "staticresources") {
            var fileOrDirname = filePathParts[j + 1];
            if(!!fileOrDirname) {
              var fileOrDirnameParts = fileOrDirname.split(".");
              srcFolder = path.join(srcFolder, fileOrDirnameParts[0]);
              outputFileDir = path.join(outputFileDir, fileOrDirnameParts[0]);
              resourceFile = srcFolder + ".resource-meta.xml";
              staticRecourceRoot = outputFileDir + ".resource-meta.xml";
              if (fs.existsSync(srcFolder)) {
                if (fs.existsSync(outputFileDir) == false) {
                  fs.mkdirSync(outputFileDir);
                }
              }
            }
            break;
          }
        }
        if (fs.existsSync(srcFolder)) {
          copydir.sync(srcFolder, outputFileDir);
        }
        if (fs.existsSync(resourceFile)) {
          fs.copyFileSync(resourceFile, staticRecourceRoot);
        }
      } else {
        // fs.copyFileSync(filePath, outputFile);
        var fileNameParts = fileName.split(".");
        var pattern = fileNameParts[0] + ".*";
        if (fileName.includes("quickAction-meta.xml")) {
          pattern = fileName.replace("quickAction-meta.xml", "*");
        }
        var associatedFilePattern = filePath.replace(fileName, pattern);
        var files = glob.sync(associatedFilePattern);
        for (var k = 0; k < files.length; k++) {
          if (fs.lstatSync(files[k]).isDirectory() == false) {
            var oneFilePath = path.join(".", files[k]);
            var oneFilePathParts = oneFilePath.split(path.sep);
            fileName = oneFilePathParts[oneFilePathParts.length - 1];
            var outputPath = path.join(outputFileDir, fileName);
            fs.copyFileSync(files[k], outputPath);
          }
        }
      }
    }
    return { content };
  }
}
