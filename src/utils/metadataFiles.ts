import ISfdxConfigs from "./global";
import FileUtils from "../../lib/fsutils";

var path = require("path");

const configs = require('./global') as ISfdxConfigs;

export default class MetadataFiles {
    apps: string[];
    objects: string[];
    fields: string[];
    layouts: string[];
    classes: string[];
    recordTypes: string[];
    listViews: string[];
    webLinks: string[];
    pages: string[];
    tabs: string[];
    profiles: string[];
    permissionSets: string[];
    translations: string[];
    validationRules: string[];
    compactLayouts: string[];
    businessProcesses: string[];
    customMetadatas: string[];
    queues: string[];
    groups: string[];
    roles: string[];
    dashboards: string[];
    emails: string[];
    reports: string[];
    metadataTypes: MetadataTypeList
    constructor() {
        this.init();
    }

    public init(): void {
        this.apps = [];
        this.objects = [];
        this.fields = [];
        this.layouts = [];
        this.classes = [];
        this.recordTypes = [];
        this.listViews = [];
        this.webLinks = [];
        this.pages = [];
        this.tabs = [];
        this.profiles = [];
        this.permissionSets = [];
        this.translations = [];
        this.validationRules = [];
        this.compactLayouts = [];
        this.businessProcesses = [];
        this.customMetadatas = [];
        this.queues = [];
        this.groups = [];
        this.roles = [];
        this.dashboards = [];
        this.emails = [];
        this.reports = [];
    }
    static getFullApiName(fileName: string): string {
        let fullName = ""
        let metadateType = MetadataFiles.getNameOfTypes(fileName)
        let splitFilepath = fileName.split(path.sep);
        if (metadateType === 'CustomField' || metadateType === 'RecordType' || metadateType === 'ListView' || metadateType === 'ValidationRule' || metadateType === 'WebLink' || metadateType === 'CompactLayout' || metadateType === 'BusinessProcess') {
            let objectName = splitFilepath[splitFilepath.length - 3];
            let fieldName = splitFilepath[splitFilepath.length - 1].split('.')[0];
            fullName = objectName.concat('.' + fieldName);
        }
        else {
            fullName = splitFilepath[splitFilepath.length - 1].split('.')[0];
        }
        return fullName
    }
    static getFullApiNameWithExtension(fileName: string): string {
        let fullName = ""
        let metadateType = MetadataFiles.getNameOfTypes(fileName)
        let splitFilepath = fileName.split(path.sep);
        if (metadateType === 'CustomField' || metadateType === 'RecordType' || metadateType === 'ListView' || metadateType === 'ValidationRule' || metadateType === 'WebLink' || metadateType === 'CompactLayout' || metadateType === 'BusinessProcess') {
            let objectName = splitFilepath[splitFilepath.length - 3];
            let fieldName = splitFilepath[splitFilepath.length - 1];
            fullName = objectName.concat('.' + fieldName);
        }
        else {
            fullName = splitFilepath[splitFilepath.length - 1];
        }
        return fullName
    }

    public static isCustomMetadata(filepath: string, name: string): boolean {
        //Asume all metadata are custom
        //the Check support only Custom field, Custom Object and Layout for Now
        let result=true
        let splitFilepath = filepath.split(path.sep);
        let componentName=splitFilepath[splitFilepath.length-1]
        componentName=componentName.substring(0, componentName.indexOf("."))
        if(name==="CustomField" || name==="CustomObject"){
            //Custom Field or Custom Object
            result= componentName.endsWith('__c') || componentName.endsWith('__mdt')
        }
        else if (name==="Layout"){
            //Layout 
            let objectName=componentName.substring(0, componentName.indexOf("-"))
            result= objectName.endsWith('__c') || objectName.endsWith('__mdt')
        }

        return result

    }
    public static getMemberNameFromFilepath(filepath: string, name: string): string {
        var member: string;
        var splitFilepath = filepath.split(path.sep);
        var lastIndex = splitFilepath.length - 1;
        if (name === 'CustomField' || name === 'RecordType' || name === 'ListView' || name === 'ValidationRule' || name === 'WebLink' || name === 'CompactLayout' || name === 'BusinessProcess') {
            var objectName = splitFilepath[lastIndex - 2];
            var fieldName = splitFilepath[lastIndex].split('.')[0];
            member = objectName.concat('.' + fieldName);
        }
        else if (name === 'Dashboard' || name === 'Document' || name === 'EmailTemplate' || name === 'Report') {
            let baseName=""
            switch(name){
                case "Dashboard":
                    baseName="dashboards"
                    break
                case "Document":
                    baseName="documents"
                    break
                case "EmailTemplate":
                    baseName="email"
                    break
                case "Report":
                    baseName="reports"
                    break
            }
            let baseIndex= filepath.indexOf(baseName)+ baseName.length
            var cmpPath= filepath.substring(baseIndex+1) // add 1 to remove the path seperator
            cmpPath=cmpPath.substring(0, cmpPath.indexOf("."))
            member=cmpPath.replace(path.sep, "/")
        }
        else if (name === "QuickAction") {
            member = splitFilepath[lastIndex].replace('.quickAction-meta.xml', '')
        }
        else if (name === "CustomMetadata") {
            member = splitFilepath[lastIndex].replace(configs.customMetadataExtension, '')
        }
        // Add exceptions for member names that need to be treated differently using else if
        else {
            member = splitFilepath[lastIndex].split('.')[0];
        }
        return member;
    }
    //Determine the name based on the filename, for example, apexClass, customObject etc
    //NEED TO TAKE INTO CONSIDERATION OTHER TYPES AS WELL, FOR E.G, OBJECT TRANSLATIONS - NOT COMPLETED
    public static getNameOfTypes(filename: string): string {
        let name: string;
        if (filename.endsWith('.settings-meta.xml')) {
            name = 'AccountSettings';
        }
        else if (filename.endsWith('.actionLinkGroupTemplate-meta.xml')) {
            name = 'ActionLinkGroupTemplate';
        }
        else if (filename.endsWith(configs.classExtension)) {
            name = 'ApexClass';
        }
        else if (filename.endsWith('.component-meta.xml')) {
            name = 'ApexComponent';
        }
        else if (filename.endsWith(configs.pageExtension)) {
            name = 'ApexPage';
        }
        else if (filename.endsWith('.trigger-meta.xml')) {
            name = 'ApexTrigger';
        }
        else if (filename.endsWith('.approvalProcess-meta.xml')) {
            name = 'ApprovalProcess';
        }
        else if (filename.endsWith('.assignmentRules-meta.xml')) {
            name = 'AssignmentRules';
        }
        else if (filename.endsWith('.authProvider-meta.xml')) {
            name = 'AuthProvider';
        }
        else if (filename.endsWith(configs.appExtension)) {
            name = 'CustomApplication';
        }
        else if (filename.endsWith('.customApplicationComponent-meta.xml')) {
            name = 'CustomApplicationComponent';
        }
        else if (filename.endsWith(configs.objectExtension)) {
            name = 'CustomObject';
        }
        else if (filename.endsWith(configs.customMetadataExtension)) {
            name = 'CustomMetadata';
        }
        else if (filename.endsWith('.field-meta.xml')) {
            name = 'CustomField';
        }
        else if (filename.endsWith('.webLink-meta.xml')) {
            name = 'WebLink';
        }
        else if (filename.endsWith('.listView-meta.xml')) {
            name = 'ListView';
        }
        else if (filename.endsWith('.validationRule-meta.xml')) {
            name = 'ValidationRule';
        }
        else if (filename.endsWith(configs.tabExtension)) {
            name = 'CustomTab';
        }
        else if (filename.endsWith(configs.dashboardExtension)|| filename.endsWith(configs.dashboardFolderExtension)) {
            name = 'Dashboard';
        }
        else if (filename.endsWith('.document-meta.xml')) {
            name = 'Document';
        }
        else if (filename.endsWith(configs.emailExtension) || filename.endsWith(configs.emailFolderExtension)) {
            name = 'EmailTemplate';
        }
        else if (filename.endsWith('.delegateGroup-meta.xml')) {
            name = 'DelegateGroup';
        }
        else if (filename.endsWith('.duplicateRule-meta.xml')) {
            name = 'DuplicateRule';
        }
        else if (filename.endsWith('.objectTranslation-meta.xml')) {
            name = 'CustomObjectTranslation';
        }
        else if (filename.endsWith('.recordType-meta.xml')) {
            name = 'RecordType';
        }
        else if (filename.endsWith('.layout-meta.xml')) {
            name = 'Layout';
        }
        else if (filename.endsWith('.quickAction-meta.xml')) {
            name = 'QuickAction';
        }
        else if (filename.endsWith(configs.reportExtension)|| filename.endsWith(configs.reportFolderExtension)) {
            name = 'Report';
        }
        else if (filename.endsWith('.resource-meta.xml')) {
            name = 'StaticResource';
        }
        else if (filename.endsWith('.translations-meta.xml')) {
            name = 'Translations';
        }
        else if (filename.endsWith(configs.roleExtension)) {
            name = 'Role';
        }
        else if (filename.endsWith(configs.queueExtension)) {
            name = 'Queue';
        }
        else if (filename.endsWith(configs.groupExtension)) {
            name = 'Group';
        }
        else if (filename.endsWith(configs.profileExtension)) {
            name = 'Profile';
        }
        else if (filename.endsWith(configs.permsetExtension)) {
            name = 'PermissionSet';
        }
        else if (filename.endsWith(configs.siteExtension)) {
            name = 'CustomSite';
        }
        return name;
    }
    public loadComponents(srcFolder: string, excludedFolders: string[]): void {
        var me: MetadataFiles = this;
        var metadataFiles: string[] = FileUtils.getAllFilesSync(srcFolder, excludedFolders);
        if (Array.isArray(metadataFiles) && metadataFiles.length > 0) {
            metadataFiles.forEach(metadataFile => {
                if (metadataFile.endsWith(configs.appExtension)) {
                    me.apps.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.classExtension)) {
                    me.classes.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.fieldExtension)) {
                    me.fields.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.layoutExtension)) {
                    me.layouts.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.objectExtension)) {
                    me.objects.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.pageExtension)) {
                    me.pages.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.permsetExtension)) {
                    me.permissionSets.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.profileExtension)) {
                    me.profiles.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.recordTypeExtension)) {
                    me.recordTypes.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.listViewExtension)) {
                    me.listViews.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.webLinkExtension)) {
                    me.webLinks.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.tabExtension)) {
                    me.tabs.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.translationExtension)) {
                    me.translations.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.validationRuleExtension)) {
                    me.validationRules.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.compactLayoutExtension)) {
                    me.compactLayouts.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.businessProcessExtension)) {
                    me.businessProcesses.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.customMetadataExtension)) {
                    me.customMetadatas.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.queueExtension)) {
                    me.queues.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.groupExtension)) {
                    me.groups.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.roleExtension)) {
                    me.roles.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.dashboardExtension)||metadataFile.endsWith(configs.dashboardFolderExtension)) {
                    me.dashboards.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.emailExtension)||metadataFile.endsWith(configs.emailFolderExtension)) {
                    me.emails.push(metadataFile);
                }
                if (metadataFile.endsWith(configs.reportExtension)||metadataFile.endsWith(configs.reportFolderExtension)) {
                    me.reports.push(metadataFile);
                }
            });
        }
    }
}

export class MetadataTypeList {
    AccountRelationshipShareRule: MetadataFilePath[]
    ActionLinkGroupTemplate: MetadataFilePath[]
    ActionOverride: MetadataFilePath[]
    AnalyticSnapshot: MetadataFilePath[]
    ApexClass: MetadataFilePath[]
    ApexComponent: MetadataFilePath[]
    ApexPage: MetadataFilePath[]
    ApexTrigger: MetadataFilePath[]
    AppMenu: MetadataFilePath[]
    ApprovalProcess: MetadataFilePath[]
    ArticleType: MetadataFilePath[]
    AssignmentRules: MetadataFilePath[]
    Audience: MetadataFilePath[]
    AuthProvider: MetadataFilePath[]
    AuraDefinitionBundle: MetadataFilePath[]
    AutoResponseRules: MetadataFilePath[]
    BaseSharingRule: MetadataFilePath[]
    Bot: MetadataFilePath[]
    BotVersion: MetadataFilePath[]
    BrandingSet: MetadataFilePath[]
    BusinessProcess: MetadataFilePath[]
    CallCenter: MetadataFilePath[]
    CaseSubjectParticle: MetadataFilePath[]
    Certificate: MetadataFilePath[]
    CleanDataService: MetadataFilePath[]
    CMSConnectSource: MetadataFilePath[]
    Community: MetadataFilePath[]
    CommunityTemplateDefinition: MetadataFilePath[]
    CommunityThemeDefinition: MetadataFilePath[]
    CompactLayout: MetadataFilePath[]
    ConnectedApp: MetadataFilePath[]
    ContentAsset: MetadataFilePath[]
    CorsWhitelistOrigin: MetadataFilePath[]
    CriteriaBasedSharingRule: MetadataFilePath[]
    CustomApplication: MetadataFilePath[]
    CustomApplicationComponent: MetadataFilePath[]
    CustomFeedFilter: MetadataFilePath[]
    CustomField: MetadataFilePath[]
    CustomHelpMenuSection: MetadataFilePath[]
    CustomLabel: MetadataFilePath[]
    CustomLabels: MetadataFilePath[]
    CustomObject: MetadataFilePath[]
    CustomMetadata: MetadataFilePath[]
    CustomObjectTranslation: MetadataFilePath[]
    CustomPageWebLink: MetadataFilePath[]
    CustomPermission: MetadataFilePath[]
    CustomSite: MetadataFilePath[]
    CustomTab: MetadataFilePath[]
    Dashboard: MetadataFilePath[]
    DataCategoryGroup: MetadataFilePath[]
    DelegateGroup: MetadataFilePath[]
    Document: MetadataFilePath[]
    DuplicateRule: MetadataFilePath[]
    EclairGeoData: MetadataFilePath[]
    EmailServicesFunction: MetadataFilePath[]
    EmailTemplate: MetadataFilePath[]
    EmbeddedServiceBranding: MetadataFilePath[]
    EmbeddedServiceConfig: MetadataFilePath[]
    EmbeddedServiceFlowConfig: MetadataFilePath[]
    EmbeddedServiceLiveAgent: MetadataFilePath[]
    EntitlementProcess: MetadataFilePath[]
    EntitlementTemplate: MetadataFilePath[]
    EventDelivery: MetadataFilePath[]
    EventSubscription: MetadataFilePath[]
    ExternalServiceRegistration: MetadataFilePath[]
    ExternalDataSource: MetadataFilePath[]
    FeatureParameterBoolean: MetadataFilePath[]
    FeatureParameterDate: MetadataFilePath[]
    FeatureParameterInteger: MetadataFilePath[]
    FieldSet: MetadataFilePath[]
    FlexiPage: MetadataFilePath[]
    Flow: MetadataFilePath[]
    FlowCategory: MetadataFilePath[]
    FlowDefinition: MetadataFilePath[]
    Folder: MetadataFilePath[]
    FolderShare: MetadataFilePath[]
    GlobalValueSet: MetadataFilePath[]
    GlobalValueSetTranslation: MetadataFilePath[]
    GlobalPicklistValue: MetadataFilePath[]
    Group: MetadataFilePath[]
    HomePageComponent: MetadataFilePath[]
    HomePageLayout: MetadataFilePath[]
    Index: MetadataFilePath[]
    InstalledPackage: MetadataFilePath[]
    KeywordList: MetadataFilePath[]
    Layout: MetadataFilePath[]
    Letterhead: MetadataFilePath[]
    LightningBolt: MetadataFilePath[]
    LightningComponentBundle: MetadataFilePath[]
    LightningExperienceTheme: MetadataFilePath[]
    ListView: MetadataFilePath[]
    LiveChatAgentConfig: MetadataFilePath[]
    LiveChatButton: MetadataFilePath[]
    LiveChatDeployment: MetadataFilePath[]
    LiveChatSensitiveDataRule: MetadataFilePath[]
    ManagedTopics: MetadataFilePath[]
    MatchingRule: MetadataFilePath[]
    MilestoneType: MetadataFilePath[]
    MlDomain: MetadataFilePath[]
    ModerationRule: MetadataFilePath[]
    NamedCredential: MetadataFilePath[]
    NamedFilter: MetadataFilePath[]
    Network: MetadataFilePath[]
    NetworkBranding: MetadataFilePath[]
    OwnerSharingRule: MetadataFilePath[]
    PathAssistant: MetadataFilePath[]
    PermissionSet: MetadataFilePath[]
    PlatformCachePartition: MetadataFilePath[]
    PlatformEventChannel: MetadataFilePath[]
    Portal: MetadataFilePath[]
    PostTemplate: MetadataFilePath[]
    PresenceDeclineReason: MetadataFilePath[]
    PresenceUserConfig: MetadataFilePath[]
    Profile: MetadataFilePath[]
    ProfileActionOverride: MetadataFilePath[]
    ProfilePasswordPolicy: MetadataFilePath[]
    ProfileSessionSetting: MetadataFilePath[]
    Queue: MetadataFilePath[]
    QueueRoutingConfig: MetadataFilePath[]
    QuickAction: MetadataFilePath[]
    RecommendationStrategy: MetadataFilePath[]
    RecordActionDeployment: MetadataFilePath[]
    RecordType: MetadataFilePath[]
    Report: MetadataFilePath[]
    ReportType: MetadataFilePath[]
    Role: MetadataFilePath[]
    SamlSsoConfig: MetadataFilePath[]
    Scontrol: MetadataFilePath[]
    SearchLayouts: MetadataFilePath[]
    SearchSettings: MetadataFilePath[]
    ServiceChannel: MetadataFilePath[]
    ServicePresenceStatus: MetadataFilePath[]
    SharingBaseRule: MetadataFilePath[]
    SharingReason: MetadataFilePath[]
    SharingRecalculation: MetadataFilePath[]
    SharingRules: MetadataFilePath[]
    SharingSet: MetadataFilePath[]
    SiteDotCom: MetadataFilePath[]
    Skill: MetadataFilePath[]
    SocialCustomerServiceSettings: MetadataFilePath[]
    StandardValueSet: MetadataFilePath[]
    StandardValueSetTranslation: MetadataFilePath[]
    StaticResource: MetadataFilePath[]
    SynonymDictionary: MetadataFilePath[]
    Territory: MetadataFilePath[]
    Territory2: MetadataFilePath[]
    Territory2Model: MetadataFilePath[]
    Territory2Rule: MetadataFilePath[]
    Territory2Settings: MetadataFilePath[]
    Territory2Type: MetadataFilePath[]
    TopicsForObjects: MetadataFilePath[]
    TransactionSecurityPolicy: MetadataFilePath[]
    Translations: MetadataFilePath[]
    ValidationRule: MetadataFilePath[]
    WaveApplication: MetadataFilePath[]
    WaveDashboard: MetadataFilePath[]
    WaveDataflow: MetadataFilePath[]
    WaveDataset: MetadataFilePath[]
    WaveLens: MetadataFilePath[]
    WaveTemplateBundle: MetadataFilePath[]
    WaveXmd: MetadataFilePath[]
    WebLink: MetadataFilePath[]
    Workflow: MetadataFilePath[]
}

export class MetadataFilePath {
    apiName: string
    paths: string[]
}